package zaliczonko;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystemException;

import org.apache.commons.io.ByteOrderMark;
        import org.apache.commons.io.ByteOrderParser;
        import org.apache.commons.io.HexDump;
        import org.apache.commons.io.FileSystemUtils;
        import org.apache.commons.io.FileUtils;
        import org.apache.commons.io.FilenameUtils;
        import org.apache.commons.io.LineIterator;
        import org.apache.commons.io.IOCase;

public class App
{

    public static void main( String[] args )
    {
        System.out.println( "Witaj!" );
         final String EXAMPLE_TXT_PATH =
                "C:\\pliki\\plik1.txt";

        System.out.println("Pelna sciezka do pliku plik.txt: " +
                FilenameUtils.getFullPath(EXAMPLE_TXT_PATH)); // pokazuje lokalizacje pliku

        System.out.println("Pelna nazwa pliku plik.txt: " +
                FilenameUtils.getName(EXAMPLE_TXT_PATH)); // pelna nazwa pliku

        System.out.println("Rozszerzenie pliku plik.txt: " +
                FilenameUtils.getExtension(EXAMPLE_TXT_PATH)); // samo rozszerzenie pliku

        System.out.println("Sama nazwa pliku: " +
                FilenameUtils.getBaseName(EXAMPLE_TXT_PATH)); // sama nazwa pliku

        String str1 = "To jest nowy string.";
        String str2 = "To jest kolejny nowy string, tak!";

        System.out.println("Konczy sie stringiem (case sensitive): " +
                IOCase.SENSITIVE.checkEndsWith(str1, "string."));
        System.out.println("Konczy sie stringiem (case insensitive): " +
                IOCase.INSENSITIVE.checkEndsWith(str1, "string."));

        System.out.println("Porownanie stringow: " +
                IOCase.SENSITIVE.checkEquals(str1, str2));

        ByteOrderMark x=new ByteOrderMark("cos",16);
        System.out.println( x.getBytes() );
        System.out.println( x.toString() );
        System.out.println( x.get(1) );

        HexDump y=new HexDump();

    }
}
